/*
    Copyright (C) 2019  Garrett Tucker

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

//These are the files you want shredded
//Formatted like so within the brackets: "/path/to/file", "Path/to/dir", "/etc/"
static char *files[] = {
NULL
};

//Number of iterations of shred to run
//Change the number to number of iterations you wish
static char *shredNum = "1";

//Whitelisted USB Devices
//Add the hex value of the USB devices you desire to this array.
//The values can be obtained by using the command: lsusb.
//Copy the hex value WITHOUT the : into the array. You must also prefix with 0x.
//IE: 1d6b:0002 would be put into this array as: 0x1d6b0002
static int whitelist[] = {
0x1d6b0002
};
