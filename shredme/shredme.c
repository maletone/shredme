#include <math.h>
#include <libusb-1.0/libusb.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "config.h"


//Shred Files
void shred() {
int i;

for(i = 0; files[i] != NULL; i++) {

char *shred[] = {
"/usr/bin/shred", "-f", "-u", "-n", shredNum, files[i], NULL
};

execvp(shred[0], shred);

}
char *shutdown[] = {
  "/usr/bin/shutdown", "-r", "now"  
};

execvp(shutdown[0],shutdown);

} 

//Checking if USB is on whitelist
int match_usb(struct libusb_device_descriptor desc) {

size_t whitelist_len = sizeof(whitelist)/sizeof(whitelist[0]);

if(whitelist_len == 0) {
return 1;
}

int val;

uint16_t vendor = desc.idVendor;
uint16_t product = desc.idProduct;
uint32_t result = (vendor<<16) | product;
for(int i = 0; i < whitelist_len; i++) {
//Check if device is not on whitelist
if(result != whitelist[i]) {
val = val + 1;
}
else
{
val = val + 0;
}

}

return val;

}


int main() {

int p1 = fork();

if(p1 == 0) {

libusb_device **devs;
libusb_context *ctx;
ssize_t old;
libusb_init(&ctx);
old = libusb_get_device_list(ctx, &devs);

while(1 == 1) {
libusb_device **devs;
libusb_context *ctx;
ssize_t cnt;
libusb_init(&ctx);
cnt = libusb_get_device_list(ctx, &devs);

if(cnt != old) {
for (size_t index = 0; index < cnt; ++index) {
libusb_device *device = devs[index];
struct libusb_device_descriptor desc = {0};
libusb_get_device_descriptor(device, &desc);

int z = match_usb(desc);

if (z != 0) {
shred();
}
else if (z == 0) {
old = cnt;
}


}

}

}

libusb_free_device_list(devs, 1);
libusb_exit(ctx);
}

}

