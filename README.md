<table align="center"><tr><td align="center" width="9999">
<img src="Resources/icon.png" align="center" width="150" alt="Project icon">

# ShredMe

ShredMe is a USB Anti-Forensic Kill-Switch Utility that is heavily inspired by USBKill and Silk Guardian. I have designed the program in the C Language so that this program may be run on embedded devices, and so that it may be run without extension of the Linux Kernel. Using the C Language allows for faster execution, smaller file size, and in my opinion, better hardware support via LibUSB. 

##                    Installation
This Project requires LibUSB. This is often installed by default in linux OS, and in the Windows Version I have included LibUSB by default,
but in the event it is not installed in your distro you can get the dependency by running:
```shell
sudo apt-get install libusb-1.0-0-dev
```

Make the binary:
```shell
make shredme
```

Run the program:
```shell
sudo ./shredme
```

###                          Why?

Some reasons to use this tool:

- In case the police or other thugs come busting in (or steal your laptop from you when you are at a public library, as happened to Ross). The police commonly uses a « [mouse jiggler](http://www.amazon.com/Cru-dataport-Jiggler-Automatic-keyboard-Activity/dp/B00MTZY7Y4/ref=pd_bxgy_pc_text_y/190-3944818-7671348) » to keep the screensaver and sleep mode from activating.
- You don’t want someone to add or copy documents to or from your computer via USB.
- You want to improve the security of your (encrypted) home or corporate server (e.g. Your Raspberry).

> **[!] Important**: Only start ShredMe After all desired USB Devices are plugged in or are whitlisted. The Whitelist enables free ability to put in or take out. To shred and shutdown upon device removal, you must start ShredMe with the USB plugged in already.

> **[!] Important**: Make sure to use disk encryption for all folders that contain information you want to be private. Otherwise they will get it anyway. Full disk encryption is the easiest and surest option if available

> **Tip**: Additionally, you may use a cord to attach a USB key to your wrist. Then insert the key into your computer and start usbkill. If they steal your computer, the USB will be removed and the computer shuts down immediately.

###                      Feature List
- Compatible with Linux and Windows Operating Systems.
- Shutdown the computer when there is non-whitelisted USB activity.
- Customizable via config file
- Ability to whitelist a USB device.
- Ability to change the check interval (default: 250ms).
- Shred any desired directory or drive
- Customizable number of iterations`
- Sensible defaults
